from django.test import TestCase, Client
from django.urls import reverse, resolve
from . import views

class TestViews(TestCase):
    def test_url_pengumuman(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_template_pengumuman(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response,'experience.html')

    def test_func_pengumuman(self):
        found = resolve('/story7/')
        self.assertEqual(found.func , views.pengalaman)

    def test_isi_template_experience(self):
        response = self.client.get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn("About Me", html_response)
        self.assertIn("Current activities", html_response)
        self.assertIn("Experience", html_response)
        self.assertIn("Achievements", html_response)


    