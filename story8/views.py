from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.
def search(request):
    return render(request, 'search.html')

def data(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"
    
    json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()

    return JsonResponse(json_read)