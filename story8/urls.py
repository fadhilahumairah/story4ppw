from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from . import views

app_name ='search'

urlpatterns = [
    path('',views.search, name="cari"),
    path('data/', views.data, name='books')
]