from django.test import TestCase, Client
from django.urls import reverse, resolve


# Create your tests here.
class TestStory8(TestCase):
    def test_url(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'search.html')
    def test_content(self):
        response = Client().get('/story8/')
        isi = response.content.decode('utf8')
        self.assertIn("Cari Buku", isi)
    def test_url_json(self):
        response = Client().get(reverse('search:books'))
        self.assertEqual(response.status_code, 200)
