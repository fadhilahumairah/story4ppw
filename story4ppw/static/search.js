$(document).ready(function () {
    searchBook("harry potter");
    $("#searchbook").change(function () {
        searchBook(document.getElementById("searchbook").value);
    });
});

function searchBook(toSearch) {
    $.ajax({
        url: `https://www.googleapis.com/books/v1/volumes?q=${toSearch}`,
        success: function (result) {
            document.getElementsByTagName("table")[0].innerHTML =
                `
                <thead class="bg-dark" style="color: #fff;">
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author(s)</th>
                    <th scope="col">Publisher</th>
                    <th scope="col">Published Date</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                `;
            result.items.forEach(item => {
                let row = document.createElement("tr");

                let img = document.createElement("img");
                img.src = (item.volumeInfo.imageLinks.smallThumbnail !== undefined) ? item.volumeInfo.imageLinks.smallThumbnail : "No Image";
                img.setAttribute("class", "p-3");
                row.appendChild(img);

                let title = document.createElement("td");
                title.innerText = (item.volumeInfo.title !== undefined) ? item.volumeInfo.title : "Unknown";
                title.setAttribute("style", "vertical-align: middle;");
                row.appendChild(title);

                let authors = document.createElement("td");
                authors.innerText = (item.volumeInfo.authors !== undefined) ? item.volumeInfo.authors.join(", ") : "Unknown";
                authors.setAttribute("style", "vertical-align: middle;")
                row.appendChild(authors);

                let publisher = document.createElement("td");
                publisher.innerText = (item.volumeInfo.publisher !== undefined) ? item.volumeInfo.publisher : "Unknown";
                publisher.setAttribute("style", "vertical-align: middle;")
                row.appendChild(publisher);

                let publishedDate = document.createElement("td");
                publishedDate.innerText = (item.volumeInfo.publishedDate !== undefined) ? item.volumeInfo.publishedDate : "Unkown";
                publishedDate.setAttribute("style", "vertical-align: middle;")
                row.appendChild(publishedDate);

                document.getElementsByTagName("tbody")[0].appendChild(row);
            });
        },
        error: function () {
            $(`<h1 style="color: white;">Fetching book data failed. Please try again.</h1>`).insertAfter(bookTableId);
        }
    });
}
