from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from . import views

app_name ='jadwal'

urlpatterns = [
    path('',views.schedule, name="schedule"),
    path('create_schedule/',views.create_schedule, name="create_schedule"),
    path('<str:id>/',views.lihat_jadwal, name="lihat_jadwal"),
    path('<str:id>/delete_item/',views.delete_item, name="delete_item")
]