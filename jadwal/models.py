from django.db import models


SEMESTER = ( 
    ("1", "Gasal 2019/2020"), 
    ("2", "Genap 2019/2020"), 
    ("3", "Gasal 2020/2021"), 
    ("4", "Genap 2020/2021"), 
    ("5", "Gasal 2021/2022"), 
    ("6", "Genap 2021/2022"), 
    ("7", "Gasal 2022/2023"), 
    ("8", "Genap 2022/2023"), 
) 
class Jadwal (models.Model):
    nama_matkul = models.CharField(max_length =20, null=True)
    pengajar = models.CharField(max_length =20)
    jumlah_sks = models.CharField(max_length = 10)
    deskripsi = models.CharField(max_length =60)
    semester = models.CharField(max_length =20, choices = SEMESTER, default="3" )
    ruang = models.CharField(max_length=10, null=True)

    def __str__(self):
        return "{}".format(self.nama_matkul)



