from django import forms
from . import models

class Input_Form(forms.ModelForm):
    class Meta:
        model = models.Jadwal
        fields = "__all__"
        
    nama_matkul = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "editform",
                "max_length" : 50,
                "required"  : True,
                "placeholder"   : "Subjects"
            }
        )
    )

    pengajar = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                 "class" : "editform",
                "required"  : True,
                "placeholder" : "Lecturer",
            }
        )
    )

    jumlah_sks = forms.CharField(
        widget= forms.TextInput(
            attrs = {
                "class" : "editform",
                "required" : True,
                "placeholder" : "SKS"
            }
        )
    )

    deskripsi= forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "editform editdescription",
                "placeholder" : "Description",
                "required"  : True,
            }
        )
    )

    

    ruang= forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "editform",
                "placeholder" : "Classroom",
                "required"  : True,
            }
        )
    )

    
