from django.http import HttpResponse
from django.shortcuts import render,redirect
from .models import Jadwal
from . import forms


def schedule(request):
    jadwal = Jadwal.objects.all()
    response = {
        'jadwal' :jadwal,
    }

    return render(request,'schedule.html', response)

def create_schedule(request):
    form = forms.Input_Form()
    if (request.method == "POST"):
        form = forms.Input_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal:schedule')
        else:
        # Added else statment
            msg = 'Errors: %s' % form.errors.as_text()
            return HttpResponse(msg, status=400)
    return render(request,'create_schedule.html', {'form':form} )

def lihat_jadwal(request,id):
    objek = Jadwal.objects.get(id=id)
    response = {
        'objek' : objek,
    }
    return render (request, 'detail.html', response)

def delete_item(request,id):
    Jadwal.objects.get(id = id).delete()
    return redirect('jadwal:schedule')
