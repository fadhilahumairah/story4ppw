from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *
from .forms import *
from django.apps import apps
from .apps import Story9Config
from django.http import HttpRequest



# Create your tests here.
class Story9Test(TestCase):

    def test_app_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)

    def test_function_login(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, login)

    def test_function_logout(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, logout)
        
    def test_function_signup(self):
        found = resolve('/story9/signup/')
        self.assertEqual(found.func, signup)

    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_login_page_url_is_exist(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_is_using_correct_html(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_page_template_logout(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code,302)
    
    def test_story9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'story9.html')




    def test_signup_page_url_is_exist(self):
        response = Client().get('/story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')

    def test_form_post(self):
        signUp_form = SignUpForm(data={'email':'abcd@gmail.com', 'full_name': 'abcd'})
        self.assertFalse(signUp_form.is_valid())
        self.assertEqual(signUp_form.cleaned_data['email'],"abcd@gmail.com")
        self.assertEqual(signUp_form.cleaned_data['full_name'],"abcd") 
