from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout


# Create your views here.
def index(request):
    return render(request, 'story9.html')

def login(request):
    if request.method == 'POST': 
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            
            return redirect('/story9/')
        else:
            messages.error(request, 'Username or password is incorrect')
    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
  
    return redirect('/story9/')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request, user)
            messages.success(request, "Successfully created a user with username {}".format(username))
            return redirect('/story9/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})
