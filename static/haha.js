$( function() {
    $( "#accordion" ).accordion({
      header: "> div > h3",
      collapsible: true,
  
      heightStyle: "content",
      icons:false,
      active: 0
  
    });
  } );
  
  
  $( document ).ready(function() {
    setButtons();
    $(document).on('click', '.down', function(e) {
    var cCard = $(this).closest('.group');
    var tCard = cCard.next('.group');
    cCard.insertAfter(tCard);
    setButtons();
    });
    
    $(document).on('click', '.up', function(e) {
    var cCard = $(this).closest('.group');
    var tCard = cCard.prev('.group');
    cCard.insertBefore(tCard);
    setButtons();
    });
    
    function setButtons(){
        $('button').show();
        $('.group:first-child  button.up').hide();
        $('.group:last-child  button.down').hide();
    }
    
    });
  